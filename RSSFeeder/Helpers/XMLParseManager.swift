//
//  XMLParser.swift
//  RSSFeeder
//
//  Created by Vlatko Šprem on 14/05/2018.
//  Copyright © 2018 PineCone Apps. All rights reserved.
//

import Foundation

class XMLParseManager: NSObject, XMLParserDelegate {

    var parser = XMLParser()
    var currentNode = NSString()
    var feeds = NSMutableArray()
    var channel = NSMutableDictionary()
    var elements = NSMutableDictionary()
    var element = NSString()
    var fTitle = NSMutableString()
    var fLink = NSMutableString()
    var fImgURL = NSMutableString()
    var fDescription = NSMutableString()
    var fDate = NSMutableString()
    var imgWidth = NSNumber()
    var imgHeight = NSNumber()
    
    var feedImgURL = NSMutableString()
    var feedTitle = NSMutableString()
    var feedLink = NSMutableString()
    var feedDescription = NSMutableString()
    var feedDate = NSMutableString()
    
    func initWithURL(_ url :URL) -> AnyObject {
        startParser(url)
        return self
    }
    
    func startParser(_ url :URL) {
        feeds = []
        parser = XMLParser(contentsOf: url)!
        parser.delegate = self
        parser.shouldProcessNamespaces = false
        parser.shouldReportNamespacePrefixes = false
        parser.shouldResolveExternalEntities = false
        parser.parse()
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        element = elementName as NSString
        
        if  (element as NSString).isEqual(to: "image")
        {
            currentNode = element
            fImgURL = NSMutableString()
            fImgURL = ""
            fLink = NSMutableString()
            fLink = ""
        }
        if (element as NSString).isEqual(to: "channel") {
            currentNode = element
            channel =  NSMutableDictionary()
            channel = [:]
            fTitle = NSMutableString()
            fTitle = ""
            fLink = NSMutableString()
            fLink = ""
            fDescription = NSMutableString()
            fDescription = ""
        }
        else if (element as NSString).isEqual(to: "item") {
            currentNode = element
            elements =  NSMutableDictionary()
            elements = [:]
            fTitle = NSMutableString()
            fTitle = ""
            fLink = NSMutableString()
            fLink = ""
            fDescription = NSMutableString()
            fDescription = ""
            fDate = NSMutableString()
            fDate = ""
            feedTitle = NSMutableString()
            feedTitle = ""
            feedLink = NSMutableString()
            feedLink = ""
            feedDescription = NSMutableString()
            feedDescription = ""
            feedDate = NSMutableString()
            feedDate = ""
        }
        else if (element as NSString).isEqual(to: "enclosure") && attributeDict["type"]?.range(of: "image") != nil
        {
            feedImgURL = NSMutableString()
            feedImgURL = ""
            feedImgURL.append(attributeDict["url"]!)
        }
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if (elementName as NSString).isEqual(to: "image")
        {
            if fImgURL != "" {
                channel.setObject(fImgURL, forKey: "imgurl" as NSCopying)
            }
            channel.setObject(imgHeight, forKey: "imgheight" as NSCopying)
            channel.setObject(imgWidth, forKey: "imgwidth" as NSCopying)
        }
        else if (elementName as NSString).isEqual(to: "item")
        {
            if feedTitle != "" {
                elements.setObject(feedTitle, forKey: "title" as NSCopying)
            }
            if feedLink != "" {
                elements.setObject(feedLink, forKey: "link" as NSCopying)
            }
            if feedDescription != "" {
                elements.setObject(feedDescription, forKey: "description" as NSCopying)
            }
            if feedDate != "" {
                elements.setObject(feedDate, forKey: "pubDate" as NSCopying)
            }
            if feedImgURL != "" {
                elements.setObject(feedImgURL, forKey: "imgURL" as NSCopying)
            }
            
            feeds.add(elements)
        }
        else if (currentNode.isEqual(to: "channel"))
        {
            if fTitle != "" {
                channel.setObject(fTitle, forKey: "title" as NSCopying)
            }
            if fLink != "" {
                channel.setObject(fLink, forKey: "link" as NSCopying)
            }
            if fDescription != "" {
                channel.setObject(fDescription, forKey: "description" as NSCopying)
            }
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        
        let cleanedString = string.trimmingCharacters(in: NSCharacterSet.newlines)
        if element.isEqual(to: "title") && currentNode.isEqual(to: "channel"){
            fTitle.append(cleanedString)
        } else if element.isEqual(to: "link") && currentNode.isEqual(to: "channel"){
            fLink.append(cleanedString)
        } else if element.isEqual(to: "description") && currentNode.isEqual(to: "channel"){
            fDescription.append(cleanedString)
        } else if element.isEqual(to: "pubDate") && currentNode.isEqual(to: "channel"){
            fDate.append(cleanedString)
        }else if element.isEqual(to: "url") {
            fImgURL.append(cleanedString)
        }else if element.isEqual(to: "width") {
            let width:Int? = Int(cleanedString)
            if width != nil
            {
                imgWidth = NSNumber(value: width!)
            }
        }else if element.isEqual(to: "height") {
            let height = Int(cleanedString)
            if height != nil
            {
                imgHeight = NSNumber(value: height!)
            }
        }else if element.isEqual(to: "title"){
            feedTitle.append(cleanedString)
        } else if element.isEqual(to: "link"){
            feedLink.append(cleanedString)
        } else if element.isEqual(to: "description"){
            feedDescription.append(cleanedString)
        } else if element.isEqual(to: "pubDate"){
            feedDate.append(cleanedString)
        }
    }
}
