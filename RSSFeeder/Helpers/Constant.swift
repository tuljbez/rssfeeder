//
//  Constant.swift
//  RSSFeeder
//
//  Created by Vlatko Šprem on 13/05/2018.
//  Copyright © 2018 PineCone Apps. All rights reserved.
//

import Foundation
import UIKit

struct RSSDefaultFeeds{
    static let URL1 = "http://www.nba.com/rss/nba_rss.xml"
    static let URL2 = "http://www.espn.com/blog/feed?blog=nflnation"
}

struct UserKeys{
    static let RSS_URLS = "rss_url_array"
    static let RSS_STORED = "rss_stored"
}

struct CustomColors{
    static let lightBlue = UIColor(red: 160/255.0, green: 240/255.0, blue: 250/255.0, alpha: 1)
    static let tableBackground = UIColor(red: 210/255.0, green: 250/255.0, blue: 255/255.0, alpha: 1)
    static let evenCellBackground = UIColor(red: 190/255.0, green: 250/255.0, blue: 255/255.0, alpha: 1)
}
