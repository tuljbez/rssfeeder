//
//  UIImage+Resize.swift
//  RSSFeeder
//
//  Created by Vlatko Šprem on 25/05/2018.
//  Copyright © 2018 PineCone Apps. All rights reserved.
//

import Foundation
import UIKit

extension UIImage{
    func resizeImage(_ size:CGSize)->UIImage{
        
        var scale:CGFloat
        if self.size.width > self.size.height
        {
            scale = CGFloat(min(size.width/self.size.width,
                                size.height/self.size.height))
        }
        else
        {
            scale = CGFloat(max(size.width/self.size.width,
                                size.height/self.size.height))
        }
        let width:CGFloat  = self.size.width * scale
        let height:CGFloat = self.size.height * scale;
        
        let rr:CGRect = CGRect(x: 0, y: 0, width: width, height: height)
        
        let imageSize:CGSize = CGSize(width: width, height: height)
        UIGraphicsBeginImageContextWithOptions(imageSize, false, 0);
        self.draw(in: rr)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return newImage!
    }
}
