//
//  RSSReader.swift
//  RSSFeeder
//
//  Created by Vlatko Šprem on 13/05/2018.
//  Copyright © 2018 PineCone Apps. All rights reserved.
//

import UIKit
import UserNotifications

class RSSReader: NSObject, XMLParserDelegate {
    
    static func getAllRSSFeeds(_ store: Bool) -> NSArray
    {
        let arrRSS = NSMutableArray()
        let arrRSSProviders = setupRSSFeeds() as! [String]
        
        for urlRSS in arrRSSProviders {
            let dict = startParser(URL(string: urlRSS)!)
            if  dict != nil
            {
                arrRSS.add(dict!)
            }
        }
        if store
        {
            UserDefaults.standard.set(arrRSS, forKey: UserKeys.RSS_STORED)
        }
        return arrRSS
    }
    
    static func startParser(_ url: URL) -> NSDictionary?
    {
        let parser: XMLParseManager = XMLParseManager().initWithURL(url) as! XMLParseManager
        if parser.feeds.count>0
        {
            parser.channel.setObject(parser.feeds, forKey: "feeds" as NSCopying)
            return parser.channel
        }
        else
        {
            return nil
        }
    }
    
    static func setupRSSFeeds() -> NSArray
    {
        let arrRSSProviders = NSMutableArray()
        arrRSSProviders.add(RSSDefaultFeeds.URL1)
        arrRSSProviders.add(RSSDefaultFeeds.URL2)
        
        let arrRSSUserProviders = UserDefaults.standard.stringArray(forKey: UserKeys.RSS_URLS)
        
        if arrRSSUserProviders != nil
        {
            for url in arrRSSUserProviders!
            {
                arrRSSProviders.add(url)
            }
        }
        return arrRSSProviders
    }
    
    static func addRSSFeed(url: String) -> Bool
    {
        let dict = RSSReader.startParser(URL(string: url)!)
        if dict != nil
        {
            let arrNewFeed = dict!["feeds"] as! NSArray
            if arrNewFeed.count > 0
            {
                var arrFeeds = [String]()
                if UserDefaults.standard.stringArray(forKey: UserKeys.RSS_URLS) != nil
                {
                    arrFeeds = UserDefaults.standard.stringArray(forKey: UserKeys.RSS_URLS)!
                }
                arrFeeds.append(url)
                UserDefaults.standard.set(arrFeeds, forKey: UserKeys.RSS_URLS)
                UserDefaults.standard.synchronize()
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            return false
        }
    }
    
    static func checkIfNewFeedExists(arrRSS: NSArray) -> [String]?
    {
        let arrRSSStored = UserDefaults.standard.array(forKey: UserKeys.RSS_STORED)!
        var arrFeeds = [String]()
        
        for dict in arrRSS
        {
            let obj1 = dict as! NSDictionary
            var newFeed:Bool = false
            for dictStored in arrRSSStored
            {
                let obj2  = dictStored as! NSDictionary
                if (obj1["title"] as! NSString).isEqual(to: (obj2["title"] as! String))
                {
                    
                    if (obj1["feeds"] as! NSArray) != (obj2["feeds"] as! NSArray)
                    {
                        newFeed = true
                        break
                    }
                }
            }
            if newFeed
            {
                arrFeeds.append(obj1["title"] as! String)
            }
        }
        return arrFeeds
    }
    
    static func checkForNewFeedsInBackground()
    {
        let arr = RSSReader.getAllRSSFeeds(false)
        let arrNewFeeds = RSSReader.checkIfNewFeedExists(arrRSS: arr)
        if arrNewFeeds!.count > 0
        {
            let titles = NSMutableString()
            for title:String in arrNewFeeds!
            {
                if titles.length>0
                {
                    titles.appendFormat("\n %@", title)
                }
                else
                {
                    titles.append(title)
                }
            }
            let content = UNMutableNotificationContent()
            
            content.title = "New RSS Feed!"
            content.body = "You have news in following feed(s): \n" + (titles as String)
            content.sound = UNNotificationSound.default()
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1, repeats: false)
            let request = UNNotificationRequest.init(identifier: "notification", content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        }
    }
}
