//
//  RSSFeedsViewController.swift
//  RSSFeeder
//
//  Created by Vlatko Šprem on 11/05/2018.
//  Copyright © 2018 PineCone Apps. All rights reserved.
//

import UIKit
import UserNotifications

class RSSFeedsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    var arr = NSArray()
    var txtField = UITextField()
    var btnAdd = UIButton()
    var tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.edgesForExtendedLayout = []
        self.title = "RSS List"
        self.view.backgroundColor = CustomColors.lightBlue
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "baseline_refresh_black_24pt"), style: .plain, target: self, action: #selector(refresh))
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.black
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.setLayout()
        arr = RSSReader.getAllRSSFeeds(true)
        if  arr.count == 0
        {
            let alert = UIAlertController(title: "Error", message: "Unable to fetch RSS feeds!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    //MARK: TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        }
        
        if  indexPath.row % 2 == 0
        {
            cell?.backgroundColor = CustomColors.evenCellBackground
        }
        else
        {
            cell?.backgroundColor = UIColor.clear
        }
        
        let dict = arr[indexPath.row] as! NSDictionary
        
        if dict["imgurl"] != nil {
            let url = NSURL(string:dict["imgurl"] as! String)
            let data = NSData(contentsOf:url! as URL)
            var image = UIImage(data:data! as Data)
            
            image = (UIImage(data:data! as Data)!).resizeImage(CGSize(width:50, height:50))
            cell?.imageView?.image = image
            cell?.imageView?.contentMode = .center
        }
        else
        {
            cell?.imageView?.image = UIImage(color: .clear, size: CGSize(width:25, height:25))
        }
        
        cell?.detailTextLabel?.text = dict["description"] as? String
        cell?.detailTextLabel?.numberOfLines = 0
        cell?.detailTextLabel?.lineBreakMode = .byWordWrapping
        
        cell?.textLabel?.text = dict["title"] as? String
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel?.lineBreakMode = .byWordWrapping
        
        cell?.accessoryType = .disclosureIndicator
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arr[indexPath.row] as! NSDictionary
        let arrFeeds = dict["feeds"] as! NSArray
        
        if arrFeeds.count>0
        {
            self.navigationController?.pushViewController(RSSDetailsViewController.init(arrFeeds: arrFeeds), animated: true)
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "No news available for selected feed!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: SetupLayout
    func setLayout()
    {
        self.view.addSubview(self.txtField)
        self.txtField.translatesAutoresizingMaskIntoConstraints = false
        self.txtField.backgroundColor = UIColor.white
        self.txtField.delegate = self
        self.txtField.layer.cornerRadius = 10
        self.txtField.layer.masksToBounds = true
        self.txtField.placeholder = "Add new RSS Link"
        
        self.view.addSubview(self.btnAdd)
        self.btnAdd.translatesAutoresizingMaskIntoConstraints = false
        self.btnAdd.backgroundColor = UIColor.clear
        self.btnAdd.setBackgroundImage(UIImage(named: "baseline_add_circle_outline_black_36pt"), for: UIControlState.normal)
        self.btnAdd.titleLabel?.textAlignment = NSTextAlignment.center
        self.btnAdd.addTarget(self, action: #selector(addRSSFeed), for: UIControlEvents.touchUpInside)
        self.btnAdd.tintColor = UIColor.black
        
        let CS_leading = NSLayoutConstraint(item: self.txtField, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 15)
        let CS_trailing = NSLayoutConstraint(item: self.txtField, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.btnAdd, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: -55)
        let CS_top = NSLayoutConstraint(item: self.txtField, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 10)
        let CS_height = NSLayoutConstraint(item: self.txtField, attribute: NSLayoutAttribute.height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
        
        let CS_btnTrailing = NSLayoutConstraint(item: self.btnAdd, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: -15)
        let CS_btnTop = NSLayoutConstraint(item: self.btnAdd, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 10)
        let CS_btnHeight = NSLayoutConstraint(item: self.btnAdd, attribute: NSLayoutAttribute.height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
        let CS_btnWidth = NSLayoutConstraint(item: self.btnAdd, attribute: NSLayoutAttribute.width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30)
        view.addConstraints([CS_leading, CS_top, CS_height, CS_trailing, CS_btnTrailing, CS_btnTop, CS_btnHeight, CS_btnWidth])
        
        self.view.addSubview(self.tableView)
        self.tableView.tableFooterView = UIView()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.backgroundColor = CustomColors.tableBackground
        
        self.tableView.translatesAutoresizingMaskIntoConstraints = false
        let CS_tableLeading = NSLayoutConstraint(item: self.tableView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant:0)
        let CS_tableTrailing = NSLayoutConstraint(item: self.tableView, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: 0)
        let CS_tableTop = NSLayoutConstraint(item: self.tableView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 50)
        let CS_tableBottom = NSLayoutConstraint(item: self.tableView, attribute: NSLayoutAttribute.bottom, relatedBy: .equal, toItem: view, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        view.addConstraints([CS_tableLeading, CS_tableTrailing, CS_tableTop, CS_tableBottom])
    }
    
    //MARK: New Feed
    @objc func addRSSFeed()
    {
        if (txtField.text!.isEmpty)
        {
            let alert = UIAlertController(title: "Error", message: "Please enter valid RSS link!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        else if !RSSReader.addRSSFeed(url: txtField.text!)
        {
            let alert = UIAlertController(title: "Error", message: "RSS link you provided is not valid!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        else
        {
            refresh()
        }
    }
    
    @objc func refresh()
    {
        self.txtField.text = ""
        arr = RSSReader.getAllRSSFeeds(true)
        tableView.reloadData()
    }
    
    //MARK: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
