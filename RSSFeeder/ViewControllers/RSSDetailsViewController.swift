//
//  RSSDetailsViewController.swift
//  RSSFeeder
//
//  Created by Vlatko Šprem on 24/05/2018.
//  Copyright © 2018 PineCone Apps. All rights reserved.
//

import UIKit

class RSSDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView?
    var arrFeeds = NSArray()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(arrFeeds: NSArray) {
        super.init(nibName: nil, bundle: nil)
        self.arrFeeds = arrFeeds
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = []
        self.tableView?.tableFooterView = UIView()
        self.tableView?.backgroundColor = CustomColors.tableBackground
        self.title = "RSS Feeds"

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFeeds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "cell")
        }
        
        if  indexPath.row % 2 == 0
        {
            cell?.backgroundColor = CustomColors.evenCellBackground
        }
        else
        {
            cell?.backgroundColor = UIColor.clear
        }
        
        let dict = arrFeeds[indexPath.row] as! NSDictionary
        
        cell?.detailTextLabel?.text = dict["description"] as? String
        cell?.detailTextLabel?.numberOfLines = 0
        cell?.detailTextLabel?.lineBreakMode = .byWordWrapping
        
        cell?.textLabel?.text = dict["title"] as? String
        cell?.textLabel?.numberOfLines = 0
        cell?.textLabel?.lineBreakMode = .byWordWrapping
        
        if dict["imgURL"] != nil
        {
            cell?.imageView?.image = UIImage(color: .clear, size: CGSize(width:25, height:25)) //Image holder
            let url = NSURL(string:dict["imgURL"] as! String)
            
            URLSession.shared.dataTask(with:url! as URL, completionHandler: { (data, response, error) -> Void in
                
                if error != nil
                {
                    return
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    let image = (UIImage(data:data! as Data)!).resizeImage(CGSize(width:50, height:50))
                    cell?.imageView?.image = image
                    cell?.imageView?.contentMode = .center
                })
            }).resume()
        }
        
        cell?.accessoryType = .disclosureIndicator
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = self.arrFeeds[indexPath.row] as! NSDictionary
        let strURL = (dict["link"] as! String).trimmingCharacters(in: NSCharacterSet.whitespaces)
        let url = NSURL(string:strURL)
        
        if UIApplication.shared.canOpenURL(url! as URL)
        {
            UIApplication.shared.open(url! as URL, options: [:]) { (status) in
        
            }
        }
        else
        {
            let alert = UIAlertController(title: "Error", message: "Terribly sorry, but the link provided is not valid :(", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        
        self.tableView?.deselectRow(at: indexPath, animated: true)
    }
}
